tool
extends Panel

export(Shader) var shader_resource setget _set_shader_resource
export(String,MULTILINE) var text = "" setget _set_text

func _ready():
	if shader_resource != null && is_inside_tree():
		$TextureRect.material.shader = shader_resource
		$TextEdit.text = shader_resource.code
		$Label.text = text

func _on_TextEdit_text_changed():
	#habrá errores al modificar si falla la compilación
	shader_resource.code = $TextEdit.text

func _set_shader_resource(sr):
	shader_resource = sr
	if shader_resource != null && Engine.editor_hint && is_inside_tree():
		$TextureRect.material.shader = shader_resource
		$TextEdit.text = shader_resource.code

func _set_text(t):
	text = t
	if t != null && is_inside_tree():
		$Label.text = text
