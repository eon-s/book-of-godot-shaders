shader_type canvas_item;

//uniform float u_time //se puede usar de parámetro y asignar por scripts o animaciones

void fragment(){
	//Godot ya incluye el mapeado del UV, pero la componente Y está invertida en 2D
	vec2 st = vec2(UV.x,1.0-UV.y);
	COLOR = vec4(st.x,st.y,0.0,1.0);
}