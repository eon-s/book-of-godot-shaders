shader_type canvas_item;

//uniform float u_time //se puede usar de parámetro y asignar por scripts o animaciones

// Plot a line on Y using a value between 0.0-1.0
float plot(vec2 st, float pct){
  return  smoothstep( pct-0.02, pct, st.y) -
          smoothstep( pct, pct+0.02, st.y);
}

void fragment(){
	vec2 st = vec2(UV.x,1.0-UV.y);
	float y = pow(st.x,5.0);

	vec3 color = vec3(y);

	float pct = plot(st,y);
	color = (1.0-pct)*color+pct*vec3(0.0,1.0,0.0);

	COLOR = vec4(color,1.0);
}