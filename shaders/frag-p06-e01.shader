shader_type canvas_item;

void fragment(){
	//Si no son uniforms tienen que ser declarados dentro de la funcion de procesamiento
	vec3 colorA = vec3(0.149,0.141,0.912);
	vec3 colorB = vec3(1.000,0.833,0.224);
	
	vec3 color = vec3(0.0);

	float pct = abs(sin(TIME));

	// Mix uses pct (a value from 0-1) to
	// mix the two colors
	color = mix(colorA, colorB, pct);

	COLOR = vec4(color,1.0);
}